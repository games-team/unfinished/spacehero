/* 
 * This file is part of Spacehero.
 * 
 * Spacehero is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Spacehero is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Spacehero.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Universe.h"



std::ostream& operator<< (std::ostream &o, const SkyObject &g) {
  o << "SkyObject: " << std::endl;
  o << "(x,y,z): " << g.x << " " << g.y << " " << g.z << std::endl;
  return o;
}

std::ostream& operator<< (std::ostream &o, const SkyMass &g) {
  o << "SkyMass: " << std::endl;
  o << "mass: " << g.mass << std::endl;
  //o << "(fx,fy,fz): " << g.fx << " " << g.fy << " " << g.fz << std::endl;
  o << "(vx,vy,vz)" << g.vx << " " << g.vy << " " << g.vz << std::endl;
  o << static_cast<SkyObject>(g) << std::endl;
  return o;
}
std::ostream& operator<< (std::ostream &o, const Goal &g) {
  o << "Goal: " << std::endl;
  o << "radius: " << g.radius << std::endl;
  o << static_cast<SkyObject>(g) << std::endl;
  return o;
}
std::ostream& operator<< (std::ostream &o, const Blackhole &g) {
  o << "Blackhole: " << std::endl;
  o << static_cast<SkyMass>(g) << std::endl;
  return o;
}
std::ostream& operator<< (std::ostream &o, const Star &g) {
  o << "Star: " << std::endl;
  o << static_cast<SkyMass>(g) << std::endl;
  return o;
}
std::ostream& operator<< (std::ostream &o, const Galaxy &g) {
  o << "Galaxy: " << std::endl;
  o << static_cast<SkyMass>(g) << std::endl;
  return o;
}
std::ostream& operator<< (std::ostream &o, const Level &l) {
  o << l.goal << std::endl;
  copy(l.holes.begin(),l.holes.end(),std::ostream_iterator<Blackhole>(o));
  copy(l.galaxies.begin(),l.galaxies.end(),std::ostream_iterator<Galaxy>(o));
  return o;
};
