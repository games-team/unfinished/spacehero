/* 
 * This file is part of Spacehero.
 * 
 * Spacehero is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Spacehero is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Spacehero.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Universe.h"

Level::Level(std::ifstream &in) :
  t0(),
  maxtime(30.0),
  lastt(0),
  m_delta(0),
  m_fpst(0),
  m_fps(0),
  holes(),
  galaxies(),
  goal(),
  seed(0)

{
  goal = Goal(in);
  while(in.good()) {
    char c;
    in >> c;
    if(!in.good()) break;
    std::cerr << "Type: " << c << std::endl;
    switch(c) {
      case 'M': 
        galaxies.push_back( Galaxy(in,true) );
        break;
      case 'G': 
        galaxies.push_back( Galaxy(in) );
        break;
      case 'H':
        holes.push_back( Blackhole(in) );
        break;
      case 'S':
        in >> seed;
        break;
    }
  }
  maxtime = 30.0;
}

Goal::Goal(std::ifstream &in)
{
  in >> x >> y >> z;
  in >> radius;
  setlevel();
};

Blackhole::Blackhole(std::ifstream &in) {
  double t;
  in >> x >> y >> z; 
  in >> t >> t >> t; 
  in >> mass; 
  radius = HOLESIZE*sqrt(mass/HOLEMEDIUMMASS);
  setlevel();
};

Galaxy::Galaxy(std::ifstream &in, bool master) :
  master(master),
  lr(true)
{
  in >> x >> y >> z; 
  in >> vx >> vy >> vz; 
  in >> mass; 
  radius = BULGESIZE;
};
